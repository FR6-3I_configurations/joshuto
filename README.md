# ⚠️ This repository is deprecated in favor of [this repository](https://gitlab.com/FR6-3I_configurations/yazi).

<div align='center'>

# Joshuto

</div>

## 📦 Dependencies (Arch)

### ❗ Required

AUR :

```shell
paru -S joshuto-bin
```

### ➕ Optional

Official :

```shell
paru -S \
  helix \
  jq \
  libreoffice-fresh \
  perl-image-exiftool \
  vlc
```

## ⚙️ Configuration

Use a [Nerd Font](https://github.com/ryanoasis/nerd-fonts) to have icons.
